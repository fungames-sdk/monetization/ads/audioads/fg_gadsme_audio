# Gadsme Audio

## Integration Steps

1) **"Install"** or **"Upload"** FG Gadsme Audio plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Follow the instructions in the **"Install External Plugin"** section to import Gadsme SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG Gadsme module from the Integration Manager window, you will find the last compatible version of Gadsme SDK in the _Assets > FunGames_Externals > AudioAds_ folder. Double click on the .unitypackage file to install it.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**

## Account and Settings

Ask your Publisher to create your app on the Gadsme account and to provide you the credentials for your app.

Once  done, you will need to add you API key in your project preferences (in the menu toolbar Gadsme>Preferences>Editor Preferences)

For more information about how to integrate Ad placements with Gadsme please follow their documentation.